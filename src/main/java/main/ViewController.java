package main;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Kujira on 2017/04/21.
 */
public class ViewController implements Initializable{


    @FXML
    private WebView webView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
}
