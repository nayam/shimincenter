package main;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Controller implements Initializable{
    private static final File OUT = new File("out.html");

    private StringProperty watchRangeProperty;
    private StringProperty centerNameProperty;
    private StringProperty hallNameProperty;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        watchRangeProperty = new SimpleStringProperty(watchRangeField.getText());
        centerNameProperty = new SimpleStringProperty(centerNameField.getText());
        hallNameProperty = new SimpleStringProperty(hallNameField.getText());

        watchRangeProperty.bind(watchRangeField.textProperty());
        centerNameProperty.bind(centerNameField.textProperty());
        hallNameProperty.bind(hallNameField.textProperty());
    }

    @FXML
    private TextField watchRangeField;
    @FXML
    private TextField centerNameField;
    @FXML
    private TextField hallNameField;

    @FXML
    private TextArea logArea;

    @FXML
    private void onStart(){
        ExecutorService service = Executors.newSingleThreadExecutor();

        service.submit(() -> {
            

           try {
                long start = System.currentTimeMillis();
                logArea.appendText("Started." + "\n");

                Map<String, String> cookies = Jsoup.connect("https://www.cm2.epss.jp/sendai/web/view/user/homeIndex.html")
                        .execute()
                        .cookies();
                Document rns = Jsoup.connect("https://www.cm2.epss.jp/sendai/web/view/user/rsvNameSearch.html")
                        .cookies(cookies)
                        .get();

                Pattern rnstcPattern = Pattern.compile("name='te-conditions' value='([^']*)'", Pattern.DOTALL);
                Matcher rnstcMatcher = rnstcPattern.matcher(rns.html());
                String rnstc;
                if (rnstcMatcher.find()) {
                    rnstc = rnstcMatcher.group(1);
                } else {
                    return;
                }

                Map<String, String> rnsForm = rns.select("form#childForm input").stream()
                        .filter(input -> {
                            String type = input.attr("type");
                            return type.equals("hidden") || type.equals("text");
                        })
                        .filter(input -> !input.id().contains("dummy"))
                        .collect(Collectors.toMap(input -> input.attr("name"), input -> {
                            if (input.attr("name").contains("textKeyword")) {
                                return centerNameProperty.get();
                            }
                            return input.val();
                        }));
                rnsForm.put("te-conditions", rnstc);
                rnsForm.put("layoutChildBody:childForm:doSearch", "上記の内容で検索する");

                Connection.Response d = Jsoup.connect("https://www.cm2.epss.jp/sendai/web/view/user/rsvNameSearch.html")
                        .cookies(cookies)
                        .data(rnsForm)
                        .method(Connection.Method.POST)
                        .execute();

                Document rnsr = d.parse();
                Element rnsrTr = rnsr.select("tbody#resultItems tr").stream()
                        .filter(tr -> tr.select("span#bnamem").text().contains(centerNameProperty.get()))
                        .findFirst()
                        .get();

                Pattern rnsrtcPattern = Pattern.compile("name='te-conditions' value='([^']*)'", Pattern.DOTALL);
                Matcher rnsrtcMatcher = rnsrtcPattern.matcher(rns.html());

                String rnsrtc;
                if (rnsrtcMatcher.find()) {
                    rnsrtc = rnsrtcMatcher.group(1);
                } else {
                    return;
                }



                Pattern bcdvPattern = Pattern.compile("javascript:this\\.form\\.bcd\\.value = '(\\d*)'", Pattern.DOTALL);
                Matcher bcdvMatcher = bcdvPattern.matcher(rnsrTr.html());
                String bcdv;
                if (bcdvMatcher.find()) {
                    bcdv = bcdvMatcher.group(1);
                } else {
                    return;
                }

                Map<String, String> rnsrForm = rnsr.select("form#childForm input").stream()
                        .filter(input -> {
                            String type = input.attr("type");
                            return type.equals("hidden") || type.equals("text");
                        })
                        .filter(input -> !input.attr("name").isEmpty())
                        .collect(Collectors.toMap(input -> input.attr("name"), input -> {
                            if (input.id().equals("bcd")) {
                                return bcdv;
                            }
                            return input.val();
                        }));
                rnsrForm.put("te-conditions", rnsrtc);
                rnsrForm.put("layoutChildBody:childForm:resultItems:0:doSelect", "選択");
                rnsrForm.put("layoutChildBody:childForm:doChangeDate", "submit");

                String result = IntStream.range(1, Integer.parseInt(watchRangeProperty.getValue())).mapToObj(i -> {
                    try {
                        LocalDateTime ldt = LocalDateTime.now().plusDays(i);
                        String dow = ldt.getDayOfWeek().toString();
                        if (dow.equals("SATURDAY") || dow.equals("SUNDAY")) {
                            String ld = ldt.toLocalDate().toString();
                            logArea.appendText(ld + "\n");

                            Pattern ldPattern = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})");
                            Matcher ldMatcher = ldPattern.matcher(ld);
                            if (ldMatcher.find()) {
                                Map<String, String> f = new HashMap<>(rnsrForm);
                                f.put("layoutChildBody:childForm:year", ldMatcher.group(1));
                                f.put("layoutChildBody:childForm:month", ldMatcher.group(2));
                                f.put("layoutChildBody:childForm:day", ldMatcher.group(3));
                                f.put("layoutChildBody:childForm:eyear", "2021");
                                f.remove("layoutChildBody:childForm:resultItemsSave");

                                System.out.println("a");
                                Document cres = Jsoup.connect("https://www.cm2.epss.jp/sendai/web/view/user/rsvNameSearchResult.html")
                                        .cookies(cookies)
                                        .data(f)
                                        .method(Connection.Method.POST)
                                        .execute()
                                        .parse();
                                System.out.println("b");

                                Element bhTable = cres.select("table.tablebg2").stream()
                                        .filter(table -> table.select("span#inamem").text().equals(hallNameProperty.get()))
                                        .findFirst()
                                        .get();

                                return bhTable.toString().replaceAll("(" + hallNameProperty.get() + ")", "$1" + ld);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return "";
                })
                        .collect(Collectors.joining());

                StringBuilder sb = new StringBuilder("<html><head><meta charset=\"UTF-8\"></head><body><h1>");
                sb.append(centerNameProperty.getValue());
                sb.append("</h1>");
                sb.append(result);
                sb.append("</body></html>");

                Files.write(OUT.toPath(), sb.toString().replaceAll("/sendai/web/", "").getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
                long stop = System.currentTimeMillis();

                logArea.appendText("Completed.(" +(stop-start) +  "ms)\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    @FXML
    private void onOpen() throws Exception{
        Stage stage = new Stage();
        WebView webView = new WebView();
        stage.setScene(new Scene(webView, 1040, 720));
        WebEngine engine = webView.getEngine();
        stage.titleProperty().bind(engine.titleProperty());
        stage.show();

        // （１）ローカルフォルダのHTMLを指定
        String url = "file:///" + OUT.getAbsolutePath();
        System.out.println(url);
        engine.load(url);
    }
}
